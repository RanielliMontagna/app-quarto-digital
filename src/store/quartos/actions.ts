import quartosSlice from './quartosSlice';

const actions = {
  ...quartosSlice.actions,
};

export default actions;
