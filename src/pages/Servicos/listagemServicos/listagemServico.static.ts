import { IColunas } from 'components';

export const colunasServicos: IColunas[] = [
  {
    id: 'nome',
    label: 'Nome',
  },
  {
    id: 'preco',
    label: 'Preço',
  },
  {
    id: 'acoes',
    label: 'Ações',
    align: 'right',
  },
];
